jester (1.0-16) unstable; urgency=medium

  * Team Upload
  * Set Rules-Requires-Root: no
  * Bump Standards-Version to 4.7.0
  * Rename files in debian/ for DebHelper 14/15 compatibility
  * Extend patch metadata

 -- Alexandre Detiste <tchet@debian.org>  Sun, 24 Nov 2024 15:21:57 +0100

jester (1.0-15) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.1.
  * Remove debian/watch because there has been no upstream activity for a long
    time now.

 -- Markus Koschany <apo@debian.org>  Tue, 23 Aug 2022 11:57:29 +0200

jester (1.0-14) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Sat, 02 Jan 2021 13:49:02 +0100

jester (1.0-13) unstable; urgency=medium

  * Drop deprecated menu file and xpm icon.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Use canonical VCS URI.

 -- Markus Koschany <apo@debian.org>  Sun, 25 Nov 2018 14:06:19 +0100

jester (1.0-12) unstable; urgency=medium

  * Update my e-mail address.
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs: Switch to cgit and use https.
  * wrap-and-sort -sa.
  * Resize jester.png to 128x128 pixel and install it to the hicolor
    directory.
  * Remove source/local-options.

 -- Markus Koschany <apo@debian.org>  Wed, 13 Apr 2016 05:59:38 +0200

jester (1.0-11) unstable; urgency=low

  * Upload to unstable.
  * [4b95c49] Update to use the current canonical Vcs-URI.
  * [09aa587] Update copyright years.
  * [de6ee4a] Add keywords to desktop file.

 -- Markus Koschany <apo@gambaru.de>  Fri, 03 May 2013 13:51:53 +0200

jester (1.0-10) experimental; urgency=low

  * New Maintainer. (Closes: #664093)
  * debian/rules:
  - Build with hardening=+all.
  * Patch Makefile and prepare it for hardening.
  * debian/copyright: The license is GPL-2+.
  * Add longtitle to menu file.
  * Remove superfluous debian/dirs file.
  * New Standards-Version 3.9.4, no changes needed.
  * Add desktop file and icon.
  * Add jester.xpm icon for debian menu.
  * Add watch file and describe the upstream situation.
  * Now Jester is maintained in a Git repository. Add corresponding Vcs-fields.
  * Add homepage field to debian/control and point to Debian's wiki.
  * New background color: Green is out, I like chocolate.

 -- Markus Koschany <apo@gambaru.de>  Sat, 27 Oct 2012 10:08:38 +0200

jester (1.0-9) unstable; urgency=low

  * QA upload.
  * debian/control, debian/compat
  - Set maintainer to Debian QA Group
  - Update Standards-Version to 3.9.3 (no other changes)
  - Set debhelper compatibility level to 9
  - Add ${misc:Depends} to binary package
  * debian/rules, debian/install, debian/manpages
  - Switch to debhelper sequencer
  * debian/source/format, debian/patches/*
  - Switch to dpkg-source 3.0 (quilt) format
  * debian/copyright
  - Switch to Copyright Format 1.0

 -- Ricardo Mones <mones@debian.org>  Mon, 02 Apr 2012 01:35:17 +0200

jester (1.0-8) unstable; urgency=low

  * Remove strip from install target in debian/rules (closes: #437239).
  * Updated FSF address in copyright file.
  * Update standards version (no changes needed).

 -- Chris Waters <xtifr@debian.org>  Sat, 11 Aug 2007 15:12:39 -0700

jester (1.0-7.1) unstable; urgency=low

  * Non-maintainer upload
  * Fix typos in manpage (Closes: #270691).
  * Change build-depends from xlibs-dev to libx11-dev (Closes: #346617).
  * Bumped Standards-Version to 3.6.2. No cahnges needed.
  * Updated DH_COMPAT to 4

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Sun, 15 Jan 2006 01:46:19 +0100

jester (1.0-7) unstable; urgency=low

  * Update for newer standards.
  * Updated the description to be a little more descriptive.
  * Changed menu to use more appropriate Games/Board section.
  * Added break statement @ line 133 to silence gcc's nagging.

 -- Chris Waters <xtifr@debian.org>  Thu, 22 May 2003 20:33:14 -0700

jester (1.0-6) unstable; urgency=low

  * updated for XFree86-4, build-dependency updated.

 -- Chris Waters <xtifr@debian.org>  Tue, 10 Apr 2001 02:33:43 -0700

jester (1.0-5) unstable; urgency=low

  * updated standards version, added Build-Depends to debian/control,
    fixed GPL location in copyright file.

 -- Chris Waters <xtifr@debian.org>  Sun,  5 Nov 2000 16:00:27 -0800

jester (1.0-4) unstable; urgency=low

  * new maintainer

 -- Chris Waters <xtifr@debian.org>  Mon,  5 Jul 1999 05:16:23 -0700

jester (1.0-3) frozen unstable; urgency=medium

  * Reupload into frozen because the distribution was changed suddenly
    *after* the last upload so it went into the wrong one.

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Tue,  3 Nov 1998 22:59:10 +0100

jester (1.0-2) unstable; urgency=low

  * Fixed calls to update-menu in postinst/postrm (closes: Bug#28764)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat, 31 Oct 1998 10:51:43 +0100

jester (1.0-1) unstable; urgency=low

  * Initial release
  * Added menu file

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Mon,  5 Oct 1998 21:56:09 +0200
